// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'part_list_async_notifier.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$partListStateAsyncNotifierHash() =>
    r'31cdfe2a8b2a3aa38c16068fb774a716d7fb9c21';

/// See also [PartListStateAsyncNotifier].
@ProviderFor(PartListStateAsyncNotifier)
final partListStateAsyncNotifierProvider = AutoDisposeAsyncNotifierProvider<
    PartListStateAsyncNotifier, List<Part>>.internal(
  PartListStateAsyncNotifier.new,
  name: r'partListStateAsyncNotifierProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$partListStateAsyncNotifierHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$PartListStateAsyncNotifier = AutoDisposeAsyncNotifier<List<Part>>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, inference_failure_on_uninitialized_variable, inference_failure_on_function_return_type, inference_failure_on_untyped_parameter
