import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:khatma/src/common/utils/collection_utils.dart';
import 'package:khatma/src/features/khatma/domain/khatma.dart';
import 'package:flutter/material.dart';

class KhatmaNotifier extends ChangeNotifier {
  Khatma _khatma;

  Khatma get khatma => _khatma;

  KhatmaNotifier(this._khatma);

  void withKhatma(Khatma khatma) {
    _khatma = khatma;
    notifyListeners();
  }

  void completeParts(List<int> partIds) {
    List<KhatmaPart> completedParts =
        isNotEmpty(khatma.parts) ? List<KhatmaPart>.from(khatma.parts!) : [];

    for (var partId in partIds) {
      int index = completedParts.indexWhere((part) => part.id == partId);
      if (index != -1) {
        completedParts[index] =
            completedParts[index].copyWith(finishedDate: DateTime.now());
      } else {
        completedParts
            .add(KhatmaPart(id: partId, finishedDate: DateTime.now()));
      }
    }
    _khatma = _khatma.copyWith(parts: completedParts);
    notifyListeners();
  }
}

final khatmaDetailsProvider = ChangeNotifierProvider<KhatmaNotifier>((ref) {
  return KhatmaNotifier(initKhatma());
});

Khatma initKhatma() {
  return Khatma(
    name: '',
    unit: SplitUnit.hizb,
    createDate: DateTime.now(),
    share: KhatmaShareType.private,
    recurrence: Recurrence(
      scheduler: KhatmaScheduler.never,
      startDate: DateTime.now(),
      endDate: DateTime.now().add(const Duration(days: 30)),
      unit: RecurrenceUnit.month,
      occurrence: 0,
    ),
    style: const KhatmaStyle(
      color: '',
      icon: '',
    ),
  );
}
