isEmpty(List? list) => list == null || list.isEmpty;
isNotEmpty(List? list) => list != null && list.isNotEmpty;
