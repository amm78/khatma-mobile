import 'dart:math';

import 'package:khatma/src/features/khatma/domain/khatma.dart';
import 'package:khatma/src/features/khatma/presentation/common/khatma_images.dart';
import 'package:khatma/src/features/khatma/presentation/common/khatma_utils.dart';

var random = Random();

/// Test khatmat to be used until a data source is implemented
var kTestKhatmat = [
  Khatma(
    id: '1',
    name: 'Khatmati sourat',
    description: '',
    unit: SplitUnit.sourat,
    parts: [
      KhatmaPart(id: 1, userName: "Ahmed", finishedDate: DateTime.now()),
      KhatmaPart(id: 2, userName: 'Ahmed', finishedDate: DateTime.now()),
      KhatmaPart(id: 3, userName: 'Ahmed', finishedDate: DateTime.now()),
    ],
    createDate: DateTime.parse("2022-12-26 13:27:00"),
    lastRead: DateTime.parse("2023-01-01 13:27:00"),
    recurrence: Recurrence(
        scheduler: KhatmaScheduler.custom,
        startDate: DateTime.now(),
        endDate: DateTime.now().add(const Duration(days: 365))),
    style: KhatmaStyle(
      color: khatmaColorHexList[random.nextInt(khatmaColorHexList.length)],
      icon: imagesNames[random.nextInt(imagesNames.length)],
    ),
    share: KhatmaShareType.private,
  ),
  Khatma(
    id: '2',
    name: 'Ramadan 2023',
    description: 'A l' 'ocasion de ramadan 2023',
    unit: SplitUnit.hizb,
    parts: [
      KhatmaPart(id: 1, userName: 'Ahmed', finishedDate: DateTime.now()),
      KhatmaPart(id: 2, userName: 'Ahmed', finishedDate: DateTime.now()),
      KhatmaPart(id: 3, userName: 'Ahmed', finishedDate: DateTime.now()),
    ],
    createDate: DateTime.parse("2023-01-15 13:27:00"),
    lastRead: DateTime.parse("2023-02-08 08:00:00"),
    recurrence: Recurrence(
        scheduler: KhatmaScheduler.never,
        startDate: DateTime.now(),
        endDate: DateTime.now().add(const Duration(days: 365))),
    style: KhatmaStyle(
      color: khatmaColorHexList[random.nextInt(khatmaColorHexList.length)],
      icon: imagesNames[random.nextInt(imagesNames.length)],
    ),
    share: KhatmaShareType.public,
  ),
  Khatma(
    id: '3',
    name: 'Khatma Mensuel',
    description:
        'This code provides a basic setup to create two buttons. The first button is a filled button with an upgrade icon, and the second one is an outlined button with a heart icon. Adjustments can be made based on your exact design and requirements',
    unit: SplitUnit.sourat,
    parts: [
      KhatmaPart(id: 1, userName: 'Ahmed', finishedDate: DateTime.now()),
      KhatmaPart(id: 2, userName: 'Ahmed', finishedDate: DateTime.now()),
      KhatmaPart(id: 3, userName: 'Ahmed', finishedDate: DateTime.now()),
    ],
    createDate: DateTime.parse("2023-01-01 10:15:00"),
    lastRead: DateTime.now(),
    recurrence: Recurrence(
        scheduler: KhatmaScheduler.never,
        startDate: DateTime.now(),
        endDate: DateTime.now().add(const Duration(days: 365))),
    style: KhatmaStyle(
      color: khatmaColorHexList[random.nextInt(khatmaColorHexList.length)],
      icon: imagesNames[random.nextInt(imagesNames.length)],
    ),
    share: KhatmaShareType.group,
  ),
  Khatma(
    id: '4',
    name: 'Mosquée plaisir',
    description: 'Comunité plaisir ',
    unit: SplitUnit.hizb,
    createDate: DateTime.parse("2022-12-01 10:15:00"),
    recurrence: Recurrence(
        scheduler: KhatmaScheduler.never,
        startDate: DateTime.now(),
        endDate: DateTime.now().add(const Duration(days: 365))),
    style: KhatmaStyle(
      color: khatmaColorHexList[random.nextInt(khatmaColorHexList.length)],
      icon: imagesNames[random.nextInt(imagesNames.length)],
    ),
    share: KhatmaShareType.private,
  ),
  Khatma(
    id: '5',
    name: 'Khatma Joumouaa',
    description: 'Lecture chque vendredi',
    unit: SplitUnit.hizb,
    createDate: DateTime.parse("2022-10-01 10:15:00"),
    recurrence: Recurrence(
        scheduler: KhatmaScheduler.never,
        startDate: DateTime.now(),
        endDate: DateTime.now().add(const Duration(days: 365))),
    style: KhatmaStyle(
      color: khatmaColorHexList[random.nextInt(khatmaColorHexList.length)],
      icon: imagesNames[random.nextInt(imagesNames.length)],
    ),
    share: KhatmaShareType.private,
  ),
  Khatma(
    id: '6',
    name: 'Khatma classique',
    description: 'Description classique',
    unit: SplitUnit.sourat,
    createDate: DateTime.parse("2022-11-01 10:15:00"),
    recurrence: Recurrence(
        scheduler: KhatmaScheduler.never,
        startDate: DateTime.now(),
        endDate: DateTime.now().add(const Duration(days: 365))),
    style: KhatmaStyle(
      color: khatmaColorHexList[random.nextInt(khatmaColorHexList.length)],
      icon: imagesNames[random.nextInt(imagesNames.length)],
    ),
    share: KhatmaShareType.private,
  ),
  Khatma(
    id: '7',
    name: 'Khatma 7',
    description: 'Description 7',
    unit: SplitUnit.juzz,
    createDate: DateTime.parse("2023-02-01 10:15:00"),
    recurrence: Recurrence(
        scheduler: KhatmaScheduler.never,
        startDate: DateTime.now(),
        endDate: DateTime.now().add(const Duration(days: 365))),
    style: KhatmaStyle(
      color: khatmaColorHexList[random.nextInt(khatmaColorHexList.length)],
      icon: imagesNames[random.nextInt(imagesNames.length)],
    ),
    share: KhatmaShareType.private,
  ),
  Khatma(
    id: '8',
    name: 'Khatma 8',
    description: 'Description 8',
    unit: SplitUnit.hizb,
    createDate: DateTime.parse("2022-12-01 10:15:00"),
    recurrence: Recurrence(
        scheduler: KhatmaScheduler.never,
        startDate: DateTime.now(),
        endDate: DateTime.now().add(const Duration(days: 365))),
    style: KhatmaStyle(
      color: khatmaColorHexList[random.nextInt(khatmaColorHexList.length)],
      icon: imagesNames[random.nextInt(imagesNames.length)],
    ),
    share: KhatmaShareType.private,
  ),
];
